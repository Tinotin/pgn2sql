use std::io::{stdin,Read};
use clearscreen;
use colored::*;

pub fn read_char() -> char {
    let mut input = [0; 1];
    stdin().read_exact(&mut input).unwrap();
    input[0] as char
}


pub fn quit() {
    clearscreen::clear().expect("failed to clear screen");
    header();
    println!("Salir !!");
//    break;
}

pub fn header () {
    println!("\n{:>20} {:—^120}","","");
    println!("\n{: ^120}","Chess Circuit");
    println!("\n{:>20} {:—^120}\n","", "");
}
pub fn options () {
    println!("\n");
    println!("{: >120} {: >15} {: >15}", "[h o 0]  Home".yellow(),  "[b Buscar]".purple(), "[q] Salir".white());
    println!("{: >140}","[ALT+Intro] Pantalla Completa Terminales Windows ");
    println!("\n");

}

pub fn footer (foot: &str){
    
println!("{:>140} {} · Pgn 2 Sql  2024", "", foot);
}

