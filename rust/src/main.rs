// use ansi_term::Color::Blue;
// use ansi_term::Color::Green;

use mysql::*;
use mysql::prelude::*;
// use mysql::{Opts, OptsBuilder};
use std::env;
use colored::*;
use clearscreen;
use commons;

use std::io::{BufRead, BufReader};
use std::fs::File;



// use ansi_term::Style;



    pub fn main () {
        // clearscreen::clear().expect("failed to clear screen");
        header();
        
        menu();
        count();
        num_lines_file();
        read_lines();
        // list();
            let opcion: char; 
        {
    
        println!("{: >20}{:—>120}", "", ""); // El UTF de U-2014
        commons::options();
        let foot = String::from("Torneos");
        commons::footer(&foot);
        opcion = commons::read_char();
    
        match opcion {
            '1' => main(),
            '2' => println!("Editar Torneo"),
            '0' => main(),
            'h' => main(),
            'q' => commons::quit(),
           _ => {}
           }
        }  
    }




fn header () {
    println!("\n{:>20} {:—^120}","","");
    println!("\n{: ^120}","Pgn to Sql [ Rust !! ]");
    println!("\n{: ^120}","Partidas");
    println!("\n{:>20} {:—^120}\n","", "");
}

fn menu () {
    print!("{: >40}", "[1] Nuevo PGN".blue());
    print!("{: >30}", "[2] Editar PGN".cyan());
    print!("{: >35}", "[4] Listar Partidas".green());
    print!("{: >30}", "[5] Eliminar PGN".red());
    print!("\n\n\n");
}


fn count () {
    dotenv::dotenv().ok();
    let db_host     = env::var("DB_HOST").expect("DB_HOST not set");
    let db_port     = env::var("DB_PORT").expect("DB_PORT not set");
    let db_user     = env::var("DB_USER").expect("DB_USER not set");
    let db_password = env::var("DB_PASSWORD").expect("DB_PASSWORD not set");
    let db_name     = env::var("DB_NAME").expect("DB_NAME not set");

    let db_url      = format!("mysql://{}:{}@{}:{}/{}", db_user, db_password, db_host, db_port, db_name);

    let opts = OptsBuilder::new()
        .ip_or_hostname(Some(db_host))
        .tcp_port(db_port.parse::<u16>().expect("Invalid DB_PORT"))
        .user(Some(db_user))
        .pass(Some(db_password))
        .db_name(Some(db_name));
    let pool = match mysql::Pool::new(opts) {
    Ok(pool) => pool,
    Err(err) => panic!("Failed to create MySQL connection pool: {}", err),
};
    let mut conn = pool.get_conn().unwrap();
// Ejecutar consulta para contar el número de registros
    conn.query_iter("SELECT COUNT(*) FROM torneos")
    .unwrap()
    .for_each(|row| {
    let query: i32  = from_row(row.unwrap());
     println!("Registros: {}", query);
    });
}

fn list() {
 dotenv::dotenv().ok();
    let db_host     = env::var("DB_HOST").expect("DB_HOST not set");
    let db_port     = env::var("DB_PORT").expect("DB_PORT not set");
    let db_user     = env::var("DB_USER").expect("DB_USER not set");
    let db_password = env::var("DB_PASSWORD").expect("DB_PASSWORD not set");
    let db_name     = env::var("DB_NAME").expect("DB_NAME not set");

    let db_url = format!("mysql://{}:{}@{}:{}/{}", db_user, db_password, db_host, db_port, db_name);

    let opts = OptsBuilder::new()
        .ip_or_hostname(Some(db_host))
        .tcp_port(db_port.parse::<u16>().expect("Invalid DB_PORT"))
        .user(Some(db_user))
        .pass(Some(db_password))
        .db_name(Some(db_name));

    let pool = match mysql::Pool::new(opts) {
    Ok(pool) => pool,
    Err(err) => panic!("Failed to create MySQL connection pool: {}", err),
};

    let mut conn = pool.get_conn().unwrap();

    println!("{id: ^10} {fecha: ^5} {hora: ^5} {torneo: ^5} {municipio: ^5} {direccion: ^5} \n",
                id="Id",
                fecha="Fecha",
                hora="Hora",
                torneo="Torneo",
                municipio="Municipio",
                direccion="Direccion",);
    conn.query_iter("SELECT id,  hora, fecha , municipio, torneo,  direccion FROM torneos")
    .unwrap()
    .for_each(|row| {
//    let query:(i32, String, String, String, String, String, String) = from_row(row.unwrap());
//     println!("{: ^10} {: ^5} {: ^5} {: ^5} {: ^5} {:<20}",
//              query.0, query.1, query.2, query.3, query.4, query.5,);
    let query:(i32, String, String, String, String, String) = from_row(row.unwrap());
     println!("{: <5}  {: <8} {: <12} {: <30} {: <50} {}",
              query.0, query.1, query.2, query.3, query.4, query.5) ;
});
}


fn num_lines_file() {
    let file = BufReader::new(File::open("game.pgn").expect("Unable to open file"));
    let mut line_count = 0;

    for _ in file.lines() {
        line_count += 1;
    }

    println!("Total lines: {}", line_count);
}

fn read_lines () {

    // Abre el archivo en modo lectura
    let file = File::open("game.pgn").expect("No se pudo abrir el archivo");

    // Crea un BufReader para leer el archivo de forma eficiente
    let reader = BufReader::new(file);

    // Número de líneas a leer
    let line_count = 0;


    let num_lines_to_read = 21;
    let mut lines_read = 0;

    // Itera sobre las líneas del archivo
    for line in reader.lines() {
        if let Ok(text) = line {
            println!("{}", text);
            lines_read += 1;

            // Sal del bucle si se han leído todas las líneas deseadas
            if lines_read >= num_lines_to_read {
                break;
            }
        } else {
            eprintln!("Error al leer la línea");
        }
    }
}



