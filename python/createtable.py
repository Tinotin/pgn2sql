#!/usr/bin/env python3

from functions import header
import mariadb

# Creacion de la Database 

# MariaDB [(none)]> create database games;
# MariaDB [(none)> grant all privileges on  games to 'player'@'localhost' identified by "password";
# MariaDB [(none)]> use games;

# Creacion la Tabla Games

# CREATE TABLE games (Id_Game int(8) NOT NULL,  Event varchar(256) DEFAULT NULL, Site varchar(256) DEFAULT NULL, Date date DEFAULT NULL,  Round int(2) DEFAULT NULL, Board int(3) DEFAULT NULL, White varchar(256) DEFAULT NULL,  WhiteTitle varchar(4) DEFAULT NULL, WhiteTeam varchar(256) DEFAULT NULL, WhiteElo varchar(4) DEFAULT NULL, WhiteRatingDiff varchar(4) DEFAULT NULL,  Black varchar(256) DEFAULT NULL, BlackTitle varchar(4) DEFAULT NULL, BlackTeam varchar(256) DEFAULT NULL,  BlackElo varchar(4) DEFAULT NULL, BlackRatingDiff varchar(4) DEFAULT NULL, Result varchar(3) DEFAULT NULL, UTCDate date DEFAULT NULL, UTCTime time DEFAULT NULL,  Variant varchar(256) DEFAULT NULL, EndTime varchar(25) DEFAULT NULL, TimeControl varchar(25) DEFAULT NULL, ECO varchar(3) DEFAULT NULL, Opening varchar(256) DEFAULT NULL, Termination varchar(25) DEFAULT NULL, Annotator varchar(50) DEFAULT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
