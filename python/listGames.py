#!/usr/bin/env python3

from os import remove
from pathlib import Path



fileName = r"games.sql"
fileObj = Path(fileName)
if fileObj.is_file(): 
    remove('games.sql')

# https://www.freecodecamp.org/news/python-open-file-how-to-read-a-text-file-line-by-line/

# with open("games.pgn"", "r") as pgnFile:

# line = pgnFile.read()
# print (line)

# Dictionary variable
Game = {
        "Id_Game":"",
        "Event":"",
        "Site":"",
        "Date":"",
        "Round":"",
        "White":"",
        "Black":"",
        "Result":"",
        "TimeControl":"",
        "PlayCount":"",
        "Board":"",
        "Termination":"",
        "FEN":"",
        "Annotator":"",
        "Mode":"",
        "WhiteTitle":"",
        "BlackTitle":"",
        "WhiteTeam":"",
        "BlackTeam":"",
        "WhiteElo":"",
        "BlackElo":"",
        "WhiteRatingDiff":"",
        "BlackRatingDiff":"",
        "UTCDate":"",
        "UTCTime":"",
        "Variant":"",
        "EndTime":"",
        "ECO":"",
        "Opening":"",
        "MoveText":""
    }


with open('games.pgn') as file:
    lines = file.readlines()
    line_count = len(lines)
    print('Total lines: {}'.format(line_count))
    
file.close()

sqlFile = open("games.sql", "a")
insertSql =  "INSERT INTO games (Event, Site, Date, Round,  White, Black, Result, TimeControl, PlayCount, Board, Termination, FEN, Annotator, Mode,  WhiteTitle, BlackTitle, WhiteTeam, BlackTeam,  WhiteElo, BlackElo,  WhiteRatingDiff,  BlackRatingDiff, UTCDate, UTCTime, Variant, EndTime, ECO, Opening, MoveText) VALUES"
insertSql += "\n"
sqlFile.write(insertSql)
print (sqlFile)
# Write the first line SQL Statement in games.sql



for line in lines:
        print (line)
        sqlFile = open("games.sql", "a")

        # Verifica que esa linea contiene Event

        if line.find('Event "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "('" + tagPgn[1] +"', "
            sqlFile.write(tagPgn2File)

        elif line.find('Site "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            sitePgn2File = "'"+ tagPgn[1] + "', "
            sqlFile.write(sitePgn2File)
            
        elif line.find('Date "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "'" + tagPgn[1] +"', "
            sqlFile.write(tagPgn2File)

        elif line.find('Round "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "'" + tagPgn[1] +"', "
            sqlFile.write(tagPgn2File)

        elif line.find('White "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "'" + tagPgn[1] +"', "
            sqlFile.write(tagPgn2File)

        elif line.find('Black "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "'" + tagPgn[1] +"', "
            sqlFile.write(tagPgn2File)

        elif line.find('Result "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "'" + tagPgn[1] +"', "
            sqlFile.write(tagPgn2File)

        elif line.find('TimeControl "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "'" + tagPgn[1] +"', "
            sqlFile.write(tagPgn2File)

        elif line.find('PlayCount "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "'" + tagPgn[1] +"', "
            sqlFile.write(tagPgn2File)

        elif line.find('Board "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "'" + tagPgn[1] +"', "
            sqlFile.write(tagPgn2File)

        elif line.find('Termination "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "'" + tagPgn[1] +"', "
            sqlFile.write(tagPgn2File)

        elif line.find('FEN "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "'" + tagPgn[1] +"', "
            sqlFile.write(tagPgn2File)

        elif line.find('Annotator "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "'" + tagPgn[1] +"', "
            sqlFile.write(tagPgn2File)

        elif line.find('Mode "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "'" + tagPgn[1] +"', "
            sqlFile.write(tagPgn2File)

        elif line.find('WhiteTitle "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "'" + tagPgn[1] +"', "
            sqlFile.write(tagPgn2File)

        elif line.find('WhiteTeam "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "'" + tagPgn[1] +"', "
            sqlFile.write(tagPgn2File)

        elif line.find('BlackTeam "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "'" + tagPgn[1] +"', "
            sqlFile.write(tagPgn2File)

        elif line.find('WhiteElo "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "'" + tagPgn[1] +"', "
            sqlFile.write(tagPgn2File)

        elif line.find('BlackElo "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "'" + tagPgn[1] +"', "
            sqlFile.write(tagPgn2File)

        elif line.find('WhiteRatingDiff "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "'" + tagPgn[1] +"', "
            sqlFile.write(tagPgn2File)

        elif line.find('BlackRatingDiff "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "'" + tagPgn[1] +"', "
            sqlFile.write(tagPgn2File)    

        elif line.find('UTCDate "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "'" + tagPgn[1] +"', "
            sqlFile.write(tagPgn2File)    

        elif line.find('UTCTime "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "'" + tagPgn[1] +"', "
            sqlFile.write(tagPgn2File)    

        elif line.find('Variant "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "'" + tagPgn[1] +"', "
            sqlFile.write(tagPgn2File)    

        elif line.find('EndTime "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "'" + tagPgn[1] +"', "
            sqlFile.write(tagPgn2File)    

        elif line.find('ECO "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "'" + tagPgn[1] +"', "
            sqlFile.write(tagPgn2File)    

        elif line.find('Termination "') == 1:
            separator = '"';
            tagPgn = line.split(separator)
            print (tagPgn[1])
            tagPgn2File = "'" + tagPgn[1] +"')"+"\n"
            sqlFile.write(tagPgn2File)    

        # elif line.find('Opening "') == 1:
        #     separator = '"';
        #     tagPgn = line.split(separator)
        #     print (tagPgn[1])
        #     tagPgn2File = "'" + tagPgn[1] +"', "
        #     sqlFile.write(tagPgn2File)

        # elif len(line) is 0:
        #     pass            


        elif line[0:2] == '1.':
            # tagPgn = line.split(separator)
            # print (tagPgn[1])
            tagPgn2File = "'" + line +"')"
            print (tagPgn2File)
            sqlFile.write(tagPgn2File)    
        
        else:
            tagPgn2File = "'" + tagPgn[1] +"')"+ "\n"
            sqlFile.write(tagPgn2File)    

        sqlFile.close()

      



